const gulp = require('gulp');
const fs = require('fs-extra');
const del = require('del');
const babel = require('babel-core');
const path = require('path');
const { Compiler } = require('level2-ember');
const Promise = require('bluebird');

// Default
gulp.task('default', [
    'compile',
    'vendor:ember', 'vendor:jquery', 'vendor:babel'
]);

// Compile
const EMBER_INPUT = 'ember';
const EMBER_OUTPUT = 'dist/js/app.js';
const EMBER_TC_PATH = path.join(__dirname, 'node_modules', 'ember-source', 'dist', 'ember-template-compiler.js');
gulp.task('compile', function() {
    let compiler = new Compiler(EMBER_INPUT, {
        templateCompiler: EMBER_TC_PATH,
        disableSourceURL: true,
        preprocessor: function(code, id) {
            let transpiled = babel.transform(code, {
                presets: ['env'],
                sourceMaps: true
            });
            
            transpiled.map.sourceRoot = 'level2-ember';
            transpiled.map.sources = [id];
            let sourcemap = new Buffer(JSON.stringify(transpiled.map), 'UTF-8').toString('base64');

            return transpiled.code + `\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,${sourcemap}`;
        }
    });

    return fs.ensureDir('dist/js')
        .then(() => compiler.compile(fs.createWriteStream(EMBER_OUTPUT)))
        .catch((err) => console.error(err));
});

// Vendor
gulp.task('vendor:ember', function() {
    return gulp.src('node_modules/ember-source/**/*')
        .pipe(gulp.dest('dist/vendor/ember-source'));
});
gulp.task('vendor:jquery', function() {
    return gulp.src('node_modules/jquery/**/*')
        .pipe(gulp.dest('dist/vendor/jquery'));
});
gulp.task('vendor:babel', function() {
    return gulp.src('node_modules/babel-polyfill/**/*')
        .pipe(gulp.dest('dist/vendor/babel-polyfill'));
});

// Watch
gulp.task('watch', function() {
    gulp.watch('ember/**/*', ['compile']);
    gulp.start(['compile']);
});

// Clean
gulp.task('clean', function() {
    return del('dist');
});
